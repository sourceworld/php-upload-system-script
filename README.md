
# PHP Upload System Script

```php
<?php
   if(isset($_FILES['files'])){
      
      //If You Want To Have a Customized Name
      $custom_name = "";
      
      //Upload Directory
      $upload_path = "Files/";
      
      //Allowed Extension List
      $extensions = array("jpeg","jpg","png","svg","mp4","mkv","m4v","mov","doc","gif","avi",

"odt","pdf","docx","txt","mp3","7z","rar","zip","tar.gz","z","deb");
   
      //Allowed Max File Size (In Kilobytes 1024kb = 1mb)
      $max_file_size = 1024;
      
      
      $errors = array();
      $file_name = $_FILES['files']['name'];
      $file_size = $_FILES['files']['size'];
      $file_tmp = $_FILES['files']['tmp_name'];
      $file_type = $_FILES['files']['type'];

      $file_ext = explode('.',$file_name);
      $file_ext = strtolower(end($file_ext));
      
      if(!in_array($file_ext,$extensions)){
         
        $errors[] = "File Tpye Not Allowed";
         
      }
      
      
      if($file_size > $max_file_size*1024){
         
         $errors[] = "File Size Must Be Excately 1mb";
         
      }
   
      if(!empty($custom_name)){

           $file_name = $custom_name.'.'.$file_ext;
          
       }
      
      
      if(file_exists($upload_path.$file_name)){
          
          
           $file_name = $upload_path.rand(0,9).'_'.$file_name;
          
       }else{
          
             $file_name = $upload_path.$file_name;


          
       }
      

     
        if(empty($errors)){

             move_uploaded_file($file_tmp,$file_name);
             echo "UPLOADED!";

        }else{


          foreach($errors as $error){

              echo $error;

          }

        }  
   }

?>
```

```html
<form action="" method="POST" enctype="multipart/form-data">
    <input type="file" name="files">
    <input type="submit" value="UPLOAD">
</form>
```